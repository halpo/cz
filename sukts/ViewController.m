//
//  ViewController.m
//  sukts
//
//  Created by Paul on 19/05/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewWillAppear:(BOOL)animated {
	[[self navigationController] setNavigationBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    if (_load) { //if returning from load screen
		[self openImage];
	} else {
        [self showHUD];
    }
}

- (void)viewDidLoad {
	[super viewDidLoad];
    
	[[self navigationController] setNavigationBarHidden:YES];
	self.view.backgroundColor = [UIColor blackColor];
    
	srand48(time(0));  //Init random seed
	chars = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";  //for URL generation
    
    //UI SETUP
    
	self.imageView.contentMode = UIViewContentModeScaleToFill;
	self.imageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BG"]];
    
	[self.but addTarget:self action:@selector(clear) forControlEvents:UIControlEventTouchUpInside];
	[self.nextBut addTarget:self action:@selector(nextPic) forControlEvents:UIControlEventTouchUpInside];
	[self.colorBut addTarget:self action:@selector(colorChange) forControlEvents:UIControlEventTouchUpInside];
	[self.saveBut addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
    [self.aspectBut addTarget:self action:@selector(toggleAspect) forControlEvents:UIControlEventTouchUpInside];
	[self.but setBackgroundColor:[UIColor clearColor]];
	[self.nextBut setBackgroundColor:[UIColor clearColor]];
	[self.colorBut setBackgroundColor:[UIColor clearColor]];
    [self.aspectBut setBackgroundColor:[UIColor clearColor]];
    
    [self.colorBut.titleLabel setFont:[UIFont fontWithName:@"Avenir-Black" size:8.0]];
    [self.saveBut.titleLabel setFont:[UIFont fontWithName:@"Avenir-Black" size:8.0]];
    [self.loadBut.titleLabel setFont:[UIFont fontWithName:@"Avenir-Black" size:8.0]];
    [self.aspectBut.titleLabel setFont:[UIFont fontWithName:@"Avenir-Black" size:8.0]];
    
    [self hideHUD];
    
	self.loadingText = [[UILabel alloc] initWithFrame:self.shimmeringView.bounds];
	self.loadingText.textAlignment = NSTextAlignmentCenter;
	self.loadingText.text = @"LOADING...";
	self.loadingText.font = [UIFont fontWithName:@"Avenir-Black" size:24.0];
    self.loadingText.textColor = [UIColor whiteColor];
	self.loadingText.backgroundColor = [UIColor clearColor];
    self.shimmeringView.backgroundColor = [UIColor clearColor];
    self.shimmeringView.contentView = self.loadingText;
    self.shimmeringView.shimmeringPauseDuration = 0.2;
    self.shimmeringView.shimmering = NO;
    
	self.loadingImage.contentMode = UIViewContentModeScaleAspectFit;
	NSString *sealPath = [[NSBundle mainBundle] pathForResource:@"seal" ofType:@"gif"];
	UIImage *sealGif = [UIImage sd_animatedGIFWithData:[NSData dataWithContentsOfFile:sealPath]];
	[self.loadingImage setImage:sealGif];
    [self hideSeal];
    
    //Has internet connection?
    internetReachable = [Reachability reachabilityForInternetConnection];

	[self showHUD];
}

- (BOOL)reachable { //Check connection
    BOOL web = internetReachable.isReachable;
    if (!web) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Webz!!" message:@"Your device requires complete worm access" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [self hideSeal];
        [alert show];
    }
    return web;
}

- (void)openImage {  //Open saved image
	[self showSeal];
    self.currentLink = self.openLink;
	self.openLink = nil;
	self.load = NO;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"link == %@",self.currentLink];
    NSArray *plip = [Item MR_findAllWithPredicate:predicate];
    Item *item = plip[0];
    if ([[item.name lowercaseString] isEqualToString:@"pure owned"]) {
        [self easter];
    }
    
    __unsafe_unretained typeof(self) weakSelf = self;
	[self.imageView setImageWithURL:[NSURL URLWithString:self.currentLink] completed: ^(UIImage *image, NSError *error, SDImageCacheType SDImageCacheTypeNone) {
        if (image) {
            [weakSelf showHUD];
            [weakSelf hideSeal];
        } else {
            [weakSelf reachable];
        }
	}];
}

- (void)clear {  //Erase all drawing
	[self.sig erase];
}

- (void)hideHUD {
	[self.but setAlpha:0.0f];
	[self.nextBut setAlpha:0.0f];
	[self.colorBut setAlpha:0.0f];
	[self.saveBut setAlpha:0.0f];
	[self.loadBut setAlpha:0.0f];
    [self.aspectBut setAlpha:0.0f];
}

- (void)showHUD {
	[self.but setAlpha:1.0f];
	[self.nextBut setAlpha:1.0f];
	[self.colorBut setAlpha:1.0f];
	[self.saveBut setAlpha:1.0f];
	[self.loadBut setAlpha:1.0f];
    [self.aspectBut setAlpha:1.0f];
	[self.view bringSubviewToFront:self.but];
	[self.view bringSubviewToFront:self.nextBut];
	[self.view bringSubviewToFront:self.colorBut];
	[self.view bringSubviewToFront:self.saveBut];
	[self.view bringSubviewToFront:self.loadBut];
    [self.view bringSubviewToFront:self.aspectBut];
}

//Loading methods
- (void)showSeal {
    [self.view bringSubviewToFront:self.loadingImage];
    [self.view bringSubviewToFront:self.shimmeringView];
    self.shimmeringView.shimmering = YES;
    [self showHUD];
}

- (void)hideSeal {
    [self.view sendSubviewToBack:self.loadingImage];
    [self.view sendSubviewToBack:self.shimmeringView];
    self.shimmeringView.shimmering = NO;
    [self showHUD];
}

//Next random image
- (void)nextPic {
    if ([self reachable]) {
        count++;
        if (count >= 15) {  //Clear cache periodically
            SDImageCache *imageCache = [SDImageCache sharedImageCache];
            [imageCache clearMemory];
            [imageCache clearDisk];
            count = 0;
        }
        self.currentLink = [NSString stringWithFormat:@"%@%@.jpg", kPrepend, [self randomStringWithLength:5]]; //generate imgur link
        NSLog(@"%@", self.currentLink);
        NSURL *url = [NSURL URLWithString:self.currentLink];

        [self showSeal];
        
        __unsafe_unretained typeof(self) weakSelf = self;
        [self.imageView setImageWithURL:url placeholderImage:nil completed: ^(UIImage *image, NSError *error, SDImageCacheType SDImageCacheTypeNone) {
            if ((image.size.width == 161.0 && image.size.height == 81)||!image) { //no image present (size of imgur placeholder)
                [weakSelf nextPic];
            } else if (error) {
                NSLog(@"Error: %@", error?:@"none");
                [weakSelf.imageView setImage:[UIImage imageNamed:@"fail"]];
                [weakSelf hideSeal];
            } else {
                [weakSelf hideSeal];
                [weakSelf showHUD];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	SDImageCache *imageCache = [SDImageCache sharedImageCache]; //Clear cache
	[imageCache clearMemory];
	[imageCache clearDisk];
}

- (BOOL)prefersStatusBarHidden {
	return YES;
}

- (void)colorChange {
    InfColorPickerController *picker = [InfColorPickerController colorPickerViewController];
    if (self.penColour) {
        picker.sourceColor = self.penColour;
    } else {
        picker.sourceColor = [UIColor blackColor];
    }
	picker.delegate = self;
    iOS7?[[UIView appearance] setTintColor:[UIColor whiteColor]]:nil;
	[picker presentModallyOverViewController: self];
}

- (void)colorPickerControllerDidFinish: (InfColorPickerController*) picker {
	self.penColour = picker.resultColor;
    [self.colorBut setTitleColor:picker.resultColor forState:UIControlStateNormal];
    CGColorRef color = [picker.resultColor CGColor];
    const CGFloat *components = CGColorGetComponents(color);
    CGFloat red = components[0];
    CGFloat green = components[1];
    CGFloat blue = components[2];
    GLKVector3 colorVect = { red, green, blue };
	[self.sig changeColor:colorVect];
    iOS7?[[UIView appearance] setTintColor:[UIColor blackColor]]:nil;
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)save {
	UIAlertView *screen = [[UIAlertView alloc] initWithTitle:@"Save Image?" message:nil delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Share", @"Copy Imgur Link", @"Save in App", nil];
	screen.tag = 1;
	[screen show];
}

- (void)save2 {
    //Takes screen capture and attaches it to a UIActivityViewController
	UIGraphicsBeginImageContext([UIScreen mainScreen].bounds.size);
	[self.view drawViewHierarchyInRect:[UIScreen mainScreen].bounds afterScreenUpdates:NO];  //iOS 7+
	[self showHUD];
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	NSArray *activityItems;
	NSString *activityText = @"Check out this one lads! hahaha";
	activityItems = @[activityText, image];
	UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];

	[self presentViewController:activityController animated:YES completion:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    //Save menu logic
	if (buttonIndex == 1 && alertView.tag == 1) {
		if (iOS7) {
			[self hideHUD];
			[self performSelector:@selector(save2) withObject:nil afterDelay:0.1];
		}
		else {
			UIAlertView *screen = [[UIAlertView alloc] initWithTitle:@"Update your phone, loser!" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[screen show];
			[self showHUD];
		}
	}
	else if (buttonIndex == 2 && alertView.tag == 1) {
		if (self.currentLink) {
			UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
			pasteboard.string = self.currentLink;
		}
	}
	else if (buttonIndex == 3 && alertView.tag == 1) {
        if (self.imageView.image) {
            UIAlertView *saveAlert = [[UIAlertView alloc] initWithTitle:@"Enter a name" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
            saveAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
            UITextField *textField = [saveAlert textFieldAtIndex:0];
            textField.placeholder = @"enter name";
            saveAlert.tag = 2;
            [saveAlert show];
        }
	}
	else if (buttonIndex == 1 && alertView.tag == 2) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@",[alertView textFieldAtIndex:0].text];
        NSArray *plip = [Item MR_findAllWithPredicate:predicate];
        if (plip.count) {
            UIAlertView *screen = [[UIAlertView alloc] initWithTitle:@"You already got 1 called that!\nChoose a different name, slimeball!" message:nil delegate:nil cancelButtonTitle:@"OK Boss!" otherButtonTitles:nil];
			[screen show];
        } else {
            UIImage *thumbnailImage = [self imageWithImage:self.imageView.image scaledToSize:CGSizeMake(100, 100)];
            Item *newItem = [Item MR_createEntity];
            newItem.name = [alertView textFieldAtIndex:0].text;
            newItem.date = [NSDate date];
            newItem.link = self.currentLink;
            newItem.thumbnail = UIImagePNGRepresentation(thumbnailImage);
            
            if ([[[alertView textFieldAtIndex:0].text lowercaseString] isEqualToString:@"pure owned"]) {
                [self easter];
            }
            
            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error){
                CGRect bounds = [[UIScreen mainScreen] bounds];
                if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
                    bounds.size = CGSizeMake(bounds.size.height, bounds.size.width);
                }
                
                UILabel *saved = [[UILabel alloc] initWithFrame:CGRectMake(0, bounds.size.height/2.0-200, bounds.size.width, 400)];
                saved.adjustsFontSizeToFitWidth = YES;
                saved.text = @"SAVED!";
                saved.backgroundColor = [UIColor clearColor];
                saved.font = [UIFont fontWithName:@"Avenir-Black" size:100.0];
                saved.textColor = [UIColor whiteColor];
                saved.shadowColor = [UIColor blackColor];
                saved.shadowOffset = CGSizeMake(1, 1);
                saved.contentMode = UIViewContentModeCenter;
                saved.transform = CGAffineTransformScale(saved.transform, 0.01, 0.01);
                saved.center = self.view.center;
                [self.view addSubview:saved];
                
                [UIView animateWithDuration:1.0 animations:^{
                    saved.transform = CGAffineTransformScale(saved.transform, 100, 100);
                    saved.center = self.view.center;
                }completion:^(BOOL done){
                    NSLog(@"Done!");
                    [saved removeFromSuperview];
                }];
            }];
        }
        
	}
}

- (void)easter {
    if (!eggz) {
		NSURL *eggFile = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"tune" ofType:@"aif"]];
		eggz = [[AVAudioPlayer alloc] initWithContentsOfURL:eggFile error:nil];
		eggz.volume = 0.8;
        eggz.numberOfLoops = -1;
	}
    
    if (eggz.isPlaying) {
        [eggz stop];
    } else {
        eggz.currentTime = 0.0f;
        [eggz prepareToPlay];
        [eggz play];
    }
}

- (void)toggleAspect {
    if (self.imageView.image) {
        if (aspect) {
            [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
            aspect = !aspect;
        } else {
            [self.imageView setContentMode:UIViewContentModeScaleToFill];
            aspect = !aspect;
        }
    }
}

//Image Resize
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
	UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
	[image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
}

//Returns random alphanumeric string
- (NSString *)randomStringWithLength:(int)len {
	NSMutableString *randomString = [NSMutableString stringWithCapacity:len];
	for (int i = 0; i < len; i++) {
		[randomString appendFormat:@"%C", [chars characterAtIndex:arc4random() % [chars length]]];
	}
	return randomString;
}

@end
