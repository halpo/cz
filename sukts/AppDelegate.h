//
//  AppDelegate.h
//  sukts
//
//  Created by Paul on 19/05/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>

#define iOS7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0

@interface AppDelegate : UIResponder <UIApplicationDelegate,AVAudioPlayerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) AVAudioPlayer *eggPlayer;

@end
