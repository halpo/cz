//
//  AppDelegate.m
//  sukts
//
//  Created by Paul on 19/05/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    iOS7?[[UIView appearance] setTintColor:[UIColor blackColor]]:nil;
    [[UINavigationBar appearance] setTitleTextAttributes: @{iOS7?NSFontAttributeName:UITextAttributeFont:[UIFont fontWithName:@"Avenir-Book" size:20.0f]}];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{iOS7?NSFontAttributeName:UITextAttributeFont:[UIFont fontWithName:@"Avenir-Book" size:16.0f]} forState:UIControlStateNormal];
    [[UITableViewHeaderFooterView appearance] setTintColor:[UIColor whiteColor]];
	[MagicalRecord setupAutoMigratingCoreDataStack];
	return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
	[MagicalRecord cleanUp];
}

@end
