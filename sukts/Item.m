//
//  Item.m
//  sukts
//
//  Created by Paul on 21/05/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "Item.h"


@implementation Item

@dynamic date;
@dynamic link;
@dynamic name;
@dynamic thumbnail;

@end
