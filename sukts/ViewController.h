//
//  ViewController.h
//  sukts
//
//  Created by Paul on 19/05/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPSSignatureView.h"
#import "UIImageView+WebCache.h"
#import "UIImage+GIF.h"
#import "Item.h"
#import "Reachability.h"
#import "FBShimmeringView.h"
#import "InfColorPicker.h"
#import "AppDelegate.h"

@interface ViewController : UIViewController <UIAlertViewDelegate,InfColorPickerControllerDelegate> {
	NSString *chars;
    int count;
    Reachability *internetReachable;
    BOOL aspect;
    AVAudioPlayer *eggz;
}
@property (strong, nonatomic) IBOutlet PPSSignatureView *sig;
@property (strong, nonatomic) IBOutlet UIButton *but;
@property (strong, nonatomic) IBOutlet UIButton *nextBut;
@property (strong, nonatomic) IBOutlet UIButton *colorBut;
@property (strong, nonatomic) IBOutlet UIButton *loadBut;
@property (strong, nonatomic) IBOutlet UIButton *aspectBut;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIButton *saveBut;
@property (strong, nonatomic) NSString *openLink;
@property (assign, nonatomic) BOOL load;
@property (strong, nonatomic) NSString *currentLink;
@property (strong, nonatomic) IBOutlet UIImageView *loadingImage;
@property (strong, nonatomic) UILabel *loadingText;
@property (strong, nonatomic) IBOutlet FBShimmeringView *shimmeringView;
@property (strong, nonatomic) UIColor *penColour;

@end
