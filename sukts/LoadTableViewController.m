//
//  LoadTableViewController.m
//  sukts
//
//  Created by Paul on 20/05/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "LoadTableViewController.h"

@interface LoadTableViewController ()

@end

@implementation LoadTableViewController

- (id)initWithStyle:(UITableViewStyle)style {
	self = [super initWithStyle:style];
	if (self) {
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
    //Get saved data
    _itemArray = [Item MR_findAllSortedBy:@"date" ascending:NO];
    //UI Setup
	[[self navigationController] setNavigationBarHidden:NO];
	self.title = @"ChoiceZone";
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    UIView *bgView = [[UIView alloc] initWithFrame:self.view.bounds];
    bgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BG"]];
    self.tableView.backgroundView = bgView;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning]; //Clear cache
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
	[imageCache clearMemory];
	[imageCache clearDisk];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return _itemArray.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *title;
    section == 0 ? title = @"Saved Images" : nil ;
    return title;
}

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *headerview = (UITableViewHeaderFooterView *)view;
    headerview.textLabel.textColor = [UIColor whiteColor];
    headerview.textLabel.font = [UIFont fontWithName:@"Avenir-Book" size:20.0];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LoadCell" forIndexPath:indexPath];
	Item *item = [_itemArray objectAtIndex:indexPath.row];
	UIImageView *thumbNail = (UIImageView *)[cell viewWithTag:4];
	if (item.thumbnail) {
		thumbNail.image = [UIImage sd_animatedGIFWithData:item.thumbnail];
	}
	else {
		thumbNail.image = nil;
	}
	UILabel *nameLabel = (UILabel *)[cell viewWithTag:5];
	nameLabel.text = item.name;
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.font = [UIFont fontWithName:@"Avenir-Book" size:18.0];
    UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
    backView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.3];
    cell.backgroundView = backView;
    cell.backgroundColor = [UIColor clearColor];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	Item *item = [_itemArray objectAtIndex:indexPath.row];
	ViewController *vc = [self.navigationController.viewControllers objectAtIndex:0];
    [vc setOpenLink:item.link];
    [vc setLoad:YES];
    [self.navigationController popToViewController:vc animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		Item *item = [_itemArray objectAtIndex:indexPath.row];
		[item MR_deleteEntity];
		[[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
		_itemArray = [Item MR_findAllSortedBy:@"date" ascending:NO];
		[self.tableView reloadData];
	}
}

@end
