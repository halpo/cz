//
//  LoadTableViewController.h
//  sukts
//
//  Created by Paul on 20/05/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface LoadTableViewController : UITableViewController

@property (strong, nonatomic) NSArray *itemArray;

@end
