//
//  LinkTableViewController.m
//  sukts
//
//  Created by Paul on 27/05/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "LinkTableViewController.h"

@interface LinkTableViewController ()

@end

@implementation LinkTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    links = [NSArray arrayWithObjects:@"Dead Good Shit",@"Brown Drinks",@"UK Dinner",nil];
    self.title = @"About";
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    UIView *bgView = [[UIView alloc] initWithFrame:self.view.bounds];
    bgView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BG"]];
    self.tableView.backgroundView = bgView;
    self.tableView.separatorColor = [UIColor clearColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return links.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LinkCell" forIndexPath:indexPath];
    UIImageView *iv = (UIImageView *)[cell viewWithTag:1];
    switch (indexPath.row) {
        case 0:
            [iv setImage:[UIImage imageNamed:@"dgsButton"]];
            break;
        case 1:
            [iv setImage:[UIImage imageNamed:@"brownButton"]];
            break;
        case 2:
            [iv setImage:[UIImage imageNamed:@"dinnerButton"]];
            break;
            
        default:
            break;
    }
    UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
    backView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.3];
    cell.backgroundView = backView;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://deadgoodshit.com"]];
            break;
        case 1:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://browndrinks.com"]];
            break;
        case 2:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://ukdinner.com"]];
            break;
            
        default:
            break;
    }
}

@end
